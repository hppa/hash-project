# hash-project

An attempt to implement SHA-2 for personal educational purposes.

Reference implementations:
https://github.com/weidai11/cryptopp/blob/master/sha.cpp

Standard:
https://csrc.nist.gov/publications/detail/fips/180/4/final

SHA-2 set of hash functions is built using the Merkle–Damgård structure